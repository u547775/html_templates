def FAILED_STAGE
def BUILD_NUMBER

pipeline 
 {
    agent any
    
	tools
	{
		jdk 'JAVA_HOME'
	} 
	options
	{
		buildDiscarder(logRotator(numToKeepStr: '10', artifactNumToKeepStr: '10'))
		
	}
	
	stages
	{
	    stage('code checkout')
		{
		 steps
			{
			 script{
			     FAILED_STAGE=env.STAGE_NAME
			     BUILD_NUMEBER=env.BUILD_NUMBER
				 }
			git([url: 'https://u547775@bitbucket.org/u547775/html_templates.git', branch: 'master', credentialsId: 'eaecdcfe-27fc-41fa-8c1f-e90b64e820ef'])
			 }
         }	
	         
	stage('Deploy HTML pages PFAdmin')
		{
		  agent {label 'PF_Admin' }
	       steps
		   {
		     sh "echo taking backup of previous file"
             sh "sudo cp -R /opt/ping/admin/pingfederate-9.3.3/pingfederate/server/default/conf/template /opt/ping/admin/pingfederate-9.3.3/pingfederate/server/default/conf/template_${env.BUILD_NUMBER} "
			 sh "echo copy new modified html version pages"
			 sh "sudo /bin/cp -p  /var/lib/Jenkins/workspace/Deploy_htmlpages/*  /opt/ping/admin/pingfederate-9.3.3/pingfederate/server/default/conf/template/"
			
	       }
         } 
		 
		 stage('Deploy HTML pages PFEngine')
		{
		  agent {label 'PF_Admin' }
	       steps
		   {
		     sh "echo taking backup of previous file"
             sh "sudo cp -R /opt/ping/engine/pingfederate-9.3.3/pingfederate/server/default/conf/template /opt/ping/engine/pingfederate-9.3.3/pingfederate/server/default/conf/template_${env.BUILD_NUMBER} "
			 sh "echo copy new modified html version pages"
			 sh "sudo /bin/cp -p  /var/lib/Jenkins/workspace/Deploy_htmlpages/*  /opt/ping/engine/pingfederate-9.3.3/pingfederate/server/default/conf/template/"
			 
	       }
        }
		stage('Deploy HTML pages PFEngine 2')
		{  agent {label 'PF_Admin' }
	       steps
		   {
		    sh "sudo scp -r /opt/Ping/PFengine/pingfederate-9.3.3/pingfederate/server/default/conf/templateec2-user@10.80.135.10:/tmp"
            sh "sudo ssh -t ec2-user@10.80.135.10 ls /tmp"
            sh "sudo ssh ec2-user@10.80.135.10 sudo cp -R /opt/Ping/PFengine/pingfederate-9.3.3/pingfederate/server/default/conf/template /opt/Ping/PFengine/pingfederate-9.3.3/pingfederate/server/default/conf/template_${env.BUILD_NUMBER}"
			sh "sudo ssh ec2-user@10.80.135.10 sudo cp -R /tmp/template /opt/ping/engine/pingfederate-9.3.3/pingfederate/server/default/conf/"
		   }
         } 		 
	}
  }  